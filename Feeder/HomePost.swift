//
//  HomePost.swift
//  billboard
//
//  Created by Nattawut Singhchai on 9/22/15.
//  Copyright © 2015 2bsimple. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import Kanna
import CoreLocation

typealias HomePostBlock = (post:HomePost!)->Void
typealias HomePostTagBlock = (tag:HomePostTag!)->Void

public class HomePostTag {
    var tagId: Int?
    var name: String?
    var linkURL: NSURL?
    
    
    
    init(json: JSON){
        if let tString = json["tagid"].string {
            tagId = Int(tString)
        }else if let iString = json["tagid"].int {
            tagId = iString
        }else{
            tagId = 0
        }
        
        name = json["name"].string?.escapedHTMLString().uppercaseString
        if let url  = json["link"].string {
            linkURL = NSURL(string: url)
        }
    }
    
    init(tagId: Int?,name: String?){
        self.tagId = tagId
        self.name = name
    }
}

class HomePost {
    
    var webTitle: String?
    var titleString: String?
    var subTitleString: String?
    var descriptionString: String?
    
    var imageURL: NSURL?
    var imageSize: CGSize?
    var linkURL: NSURL?
    var createDateString: String?
    var author: String?

    var tags: [HomePostTag]?
    var subPosts: [HomePost]?
    
    var radioURL:NSURL?
    var content: String?
    
    var htmlString:String?
    
    var sponsorImageURL: NSURL?
    var sponsorImageSize: CGSize?

    var location: CLLocationCoordinate2D?
    
    var isTmp = false
    
    init(){
        
    }
    
    init(json:JSON){
        if let sub = json.array {
             subPosts = [HomePost]()
            for j in sub {
                subPosts?.append(HomePost(json: j))
            }
        }else{
            titleString = json["title"].string?.escapedHTMLString()
            subTitleString = json["subtitle"].string
            descriptionString = json["desc"].string?.escapedHTMLString()

            author = json["author"].string
            
            if let w = json["sponsorsize"]["width"].int {
                sponsorImageSize = CGSize(width: w ,height: json["sponsorsize"]["height"].int!)
            }
            
            if let w = json["size"]["width"].int {
                imageSize = CGSize(width: w ,height: json["size"]["height"].int!)
            }
            
            if let w = json["thumb"]["width"].int {
                imageSize = CGSize(width: w ,height: json["thumb"]["height"].int!)
                imageURL = NSURL(string: json["thumb"]["src"].string!)
            }
            
            createDateString = json["date"].string
            if let link = json["link"].string {
                if link.characters.count > 4 {
                    linkURL = NSURL(string: link)
                }
            }
            
            if let link = json["url"].string {
                if link.characters.count > 4 {
                    linkURL = NSURL(string: link)
                }
            }
            
            if let img = json["img"].string {
                if img.characters.count > 4 {
                    imageURL = NSURL(string: img)
                }
            }
            
            if let img = json["sponsorimg"].string {
                if img.characters.count > 4 {
                    sponsorImageURL = NSURL(string: img)
                }
            }
            
            if let tags = json["tag"].array {
                self.tags = [HomePostTag]()
                for tag in tags {
                    self.tags!.append(HomePostTag(json: tag))
                }
            }
            if let list = json["datatitle"].array {
                self.subPosts = [HomePost]()
                for l in list {
                    self.subPosts!.append(HomePost(json: l))
                }
            }
            
            if let lat = json["lat"].string , lng = json["lng"].string {
                location = CLLocationCoordinate2DMake((lat as NSString).doubleValue, (lng as NSString).doubleValue)
                
            }
        }
        
    }
    
    class func homePostWithHTMLString(htmlString:String) -> HomePost?{
        if let $ = Kanna.HTML(html: htmlString, encoding: NSUTF8StringEncoding) {
            
            let home = HomePost()
            home.htmlString = htmlString
            home.webTitle = $.title
            
            if let node = $.body?.css("#main-content > article > div:nth-child(1) > h1").first {
                home.titleString = node.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
                print(home.titleString)
            }
            
            if let node = $.body?.css("#main-content > article > div.entry-data > h3").first {
                home.author = node.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
                print(home.author)
            }
            
            if let node = $.body?.css("#main-content > article > div.entry-data > h4").first {
                home.createDateString = node.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
                print(home.createDateString)
            }
            
            if let nodes = $.body?.css("#main-content > article > div.entry-content > div:nth-child(1) > div > div > div > div.wpb_single_image.wpb_content_element.vc_align_center > div > div > img") {
                if let node = nodes.first {
                    home.imageURL = NSURL(string: node["src"]!)
                    home.imageSize = CGSize(width: Int(node["width"]!)!, height: Int(node["height"]!)!)
                }
            }
            
            if let node = $.body?.css("audio > source").first {
                if let src = node["src"] {
                    home.radioURL = NSURL(string: src)
                }
            }
            
            if let cols = $.body?.css("div.wpb_wrapper > p") {
                print("cols \(cols.count)")
                var content = [String]()
                for col in cols {
                    if let text = col.text {
                        content.append(text)
                    }
                }
                home.content =  (content as NSArray).componentsJoinedByString("\n\n")
            }else{
                print("no path")
            }
            
            return home
        }
        return nil
    }

    class func homePostWithURL(URLString:String,didFinished:((HomePost?)->Void)) {
        HomePost.homePostWithURL(URLString, progress: nil, didFinished: didFinished)
    }
    
    class func homePostWithURL(URLString:String,progress:((Float)->Void)?,didFinished:((HomePost?)->Void)) {
        let params:[String:AnyObject] = ["ios":1,"nologin":1,"noheader":1]

        Alamofire.request(.GET, URLString,parameters:params).progress({ (_, totalBytesRead, totalBytesExpectedToRead) -> Void in
            if let progress = progress {
                dispatch_async(dispatch_get_main_queue()) {
                    let percentage = Float(totalBytesRead) / Float(totalBytesExpectedToRead)
                    progress(percentage)
                }
            }
        }).responseString(encoding: NSUTF8StringEncoding) { (r) -> Void in
            print(r.request!.URL)
            guard  r.result.value != nil else {
                didFinished(nil)
                return
            }
            didFinished(HomePost.homePostWithHTMLString(r.result.value!))
        }
    }


}
