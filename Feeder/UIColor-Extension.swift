
//
//  UIColor-Extension.swift
//  Feeder
//
//  Created by Nattawut Singhchai on 12/2/15.
//  Copyright © 2015 2bsimple. All rights reserved.
//

import UIKit

extension UIColor {
    class func mainColor() -> UIColor {
        return FD_COLOR_GLOBAL
    }
    
}

public extension UIImage {
    
    convenience init(color: UIColor, size: CGSize = CGSizeMake(1, 1)) {
        let rect = CGRectMake(0, 0, size.width, size.height)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.init(CGImage: image.CGImage!)
    }
    
    class func tmpLineWithColor(color: UIColor,lineSpace: CGFloat = 6) -> UIImage {
        let rect = CGRectMake(0, 0, 1, lineSpace + 2)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0)
        color.setFill()
        UIRectFill(CGRectMake(0, (lineSpace + 2) / 2, lineSpace + 2, 2))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return UIImage(CGImage: image.CGImage!)
        
    }
}
