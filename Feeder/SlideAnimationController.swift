//
//  SlideAnimationController.swift
//  Feeder
//
//  Created by Nattawut Singhchai on 12/9/15.
//  Copyright © 2015 2bsimple. All rights reserved.
//

import UIKit

enum SlideAnimation {
    case Presentation
    case Dismissal
}

enum SlideDirection {
    case Left
    case Right
    case Top
}

class SlideAnimationController: NSObject, UIViewControllerAnimatedTransitioning {
    
    var isPresenting: Bool
    
    var maxWidth: CGFloat?
    
    var duration:NSTimeInterval  = 0.3
    
    var direction:SlideDirection
    
    var topInset: CGFloat = 0.0
    
    init(presenting: Bool,direction: SlideDirection = .Left) {
        self.isPresenting = presenting
        self.direction = direction
    }
    
    
    // This is used for percent driven interactive transitions, as well as for container controllers that have companion animations that might need to
    // synchronize with the main animation.
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return duration
    }
    // This method can only  be a nop if the transition is interactive and not a percentDriven interactive transition.
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        if isPresenting {

            animateWithTransitionContext(transitionContext, mode: .Presentation)
        }else{
            animateWithTransitionContext(transitionContext, mode: .Dismissal)
        }
    }
    
    func animateWithTransitionContext(transitionContext: UIViewControllerContextTransitioning,mode: SlideAnimation) {
        
        let presentedControllerView  = transitionContext.viewForKey(mode == .Presentation ? UITransitionContextToViewKey : UITransitionContextFromViewKey)
        
        guard let containerView = transitionContext.containerView() else {
            return
        }
        
        if mode == .Presentation {
            if var frame = presentedControllerView?.frame {
                switch direction {
                case .Right:
                    frame.origin.x = containerView.frame.width
                case .Left:
                    frame.origin.x = -(maxWidth ?? containerView.frame.width)
                case .Top:
                    frame.origin.y = topInset-(containerView.frame.height)
                }

                frame.size = CGSize(width: maxWidth ?? containerView.frame.width, height: frame.height - topInset )
                presentedControllerView?.frame = frame
                containerView.addSubview(presentedControllerView!)
            }
        }
        
        UIView.animateWithDuration(duration, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: [.AllowUserInteraction], animations: { () -> Void in
            if var frame = presentedControllerView?.frame {
                switch self.direction {
                case .Right:
                    frame.origin.x = mode == .Presentation ? containerView.frame.width - (self.maxWidth ?? containerView.frame.width) : containerView.frame.width
                case .Left:
                    frame.origin.x = mode == .Presentation ? 0 : -(self.maxWidth ?? containerView.frame.width)
                case .Top:
                    frame.origin.y = mode == .Presentation ? 0 : self.topInset-(containerView.frame.height)
                    
                }
                presentedControllerView?.frame = frame
            }
            }) { finished -> Void in
                transitionContext.completeTransition(finished)
        }
        
    }
    

    
    // This is a convenience and if implemented will be invoked by the system when the transition context's completeTransition: method is invoked.
    func animationEnded(transitionCompleted: Bool) {
        
    }
}

class SlidePresentationController: UIPresentationController {
    var dimmingView = UIControl()
    
    var aspectWidth: CGFloat = 320
    
    var maxDimAlpha: CGFloat = 0.8
    
    override init(presentedViewController: UIViewController, presentingViewController: UIViewController) {
        super.init(presentedViewController: presentedViewController, presentingViewController: presentingViewController)
        dimmingView.backgroundColor = UIColor.whiteColor()
        dimmingView.alpha = 0.0
    }
    
    init(presentedViewController: UIViewController, presentingViewController: UIViewController,backgroundColor: UIColor = UIColor.whiteColor()) {
        
        super.init(presentedViewController: presentedViewController, presentingViewController: presentingViewController)
        
        dimmingView.backgroundColor = backgroundColor
        dimmingView.alpha = 0.0
        
    }
    
    override func presentationTransitionWillBegin() {
        guard let containerView = containerView else {
            return
        }
        
        dimmingView.frame = containerView.bounds
        dimmingView.alpha = 0.0
        
        containerView.addSubview(dimmingView)
        if let presentedView = presentedView() {
            containerView.addSubview(presentedView)
        }
        
        let transitionCoordinator = presentingViewController.transitionCoordinator()
        transitionCoordinator?.animateAlongsideTransition({ context in
            self.dimmingView.alpha = self.maxDimAlpha
        }, completion: nil)
        
        dimmingView.addTarget(self, action: "dismiss:", forControlEvents: .TouchUpInside)
    }
    
    func dismiss(sender: AnyObject) {
        
        presentedViewController.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func presentationTransitionDidEnd(completed: Bool) {
        if !completed {
            dimmingView.removeFromSuperview()
        }
    }
    
    override func dismissalTransitionWillBegin() {
        let transitionCoordinator = presentingViewController.transitionCoordinator()
        transitionCoordinator?.animateAlongsideTransition({context in
            self.dimmingView.alpha = 0.0}, completion: nil)
    }
    
    override func frameOfPresentedViewInContainerView() -> CGRect {
        if var frame = containerView?.bounds {
            let left = frame.width - aspectWidth
            frame.size.width = aspectWidth
            frame.origin.x = left
            return frame
        }
        return CGRectZero
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        guard let containerView = containerView else {
            return
        }
        coordinator.animateAlongsideTransition({context in
            self.dimmingView.frame = containerView.bounds}, completion: nil)
    }
}

