//
//  Feed.swift
//  Feeder
//
//  Created by Nattawut Singhchai on 12/7/15.
//  Copyright © 2015 2bsimple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CoreLocation

class Feed {
    
    var data = [HomePost]()
    var canLoadMore = true
    
    static var itemPerPage: Int = 200
    
    init(data:[HomePost],canLoadMore:Bool){
        self.data = data
        self.canLoadMore = canLoadMore
    }
    
    class func loadPage(page: Int,category: Category?,coordinate: CLLocationCoordinate2D?, callback: Result<Feed,NSError>->Void) {
        Feed.loadPage(page, category: category?.id, tagID: nil, tagName: nil,coordinate: coordinate, callback: callback)
    }
    
    class func loadPage(page: Int, callback: Result<Feed,NSError>->Void) {
        Feed.loadPage(page, category: nil, tagID: nil, tagName: nil,coordinate: nil, callback: callback)
    }
    
    class func loadPage(page: Int,category: Int?,tagID: String?,tagName: String?,coordinate: CLLocationCoordinate2D?,callback: (Result<Feed,NSError>)->Void) {
        
        let offset = itemPerPage * (page - 1)
        
        var param:[String:AnyObject] = ["method":"getposts","limit":itemPerPage,"offset": offset]
        
        param["tagid"] = tagID
        param["tagname"] = tagName
        param["catid"] = category
        
        if let coordinate = coordinate {
            if CLLocationCoordinate2DIsValid(coordinate) {
                param["lat"] = coordinate.latitude
                param["lng"] = coordinate.longitude
            }
        }
        
        Alamofire.request(.GET, FD_SERVICE_URL , parameters: param)
        .responseJSON { r in

            print("req: \(r.request?.URLString)")
            switch r.result {
                case .Success(let value):
                    let json = JSON(value)
                    var tmpData = [HomePost]()
                    if let d = json["data"].array {
                        for g in d {
                            tmpData.append(HomePost(json: g))
                        }
                    }
                    callback(.Success(Feed(data: tmpData, canLoadMore: tmpData.count == itemPerPage)))
                case .Failure(let error):
                    callback(.Failure(error))
            }
        }
    }
}
