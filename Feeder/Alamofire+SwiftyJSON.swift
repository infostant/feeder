//
//  Alamofire+SwiftyJSON.swift
//  Feeder
//
//  Created by Nattawut Singhchai on 12/14/15.
//  Copyright © 2015 2bsimple. All rights reserved.
//

import SwiftyJSON
import Alamofire

// MARK: - SwiftyJSON Request

extension Request {
    
    /**
     Creates a response serializer that returns a JSON object constructed from the response data using
     `NSJSONSerialization` with the specified reading options.
     
     - parameter options: The JSON serialization reading options. `.AllowFragments` by default.
     
     - returns: A JSON object response serializer.
     */
    public static func SwiftyJSONResponseSerializer(
        options options: NSJSONReadingOptions = .AllowFragments)
        -> ResponseSerializer<JSON, NSError>
    {
        return ResponseSerializer { _, response, data, error in
            guard error == nil else { return .Failure(error!) }
            
            if let response = response where response.statusCode == 204 { return .Success(JSON.null) }
            
            guard let validData = data where validData.length > 0 else {
                let failureReason = "JSON could not be serialized. Input data was nil or zero length."
                let error = Error.errorWithCode(.JSONSerializationFailed, failureReason: failureReason)
                return .Failure(error)
            }
            
            do {
                let data = try NSJSONSerialization.JSONObjectWithData(validData, options: options)
                return .Success(JSON(data))
            } catch {
                return .Failure(error as NSError)
            }
        }
    }
    
    /**
     Adds a handler to be called once the request has finished.
     
     - parameter options:           The JSON serialization reading options. `.AllowFragments` by default.
     - parameter completionHandler: A closure to be executed once the request has finished.
     
     - returns: The request.
     */
    public func responseSwiftyJSON(
        options options: NSJSONReadingOptions = .AllowFragments,
        completionHandler: Response<JSON, NSError> -> Void)
        -> Self
    {
        return response(
            responseSerializer: Request.SwiftyJSONResponseSerializer(options: options),
            completionHandler: completionHandler
        )
    }
}
