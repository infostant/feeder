//
//  Config.swift
//  Feeder
//
//  Created by Nattawut Singhchai on 12/2/15.
//  Copyright © 2015 2bsimple. All rights reserved.
//

import Foundation


import UIKit

// Main host URL

let FD_HOST_URL = "http://blog.zelection.club"

// Service URL

let FD_SERVICE_URL = FD_HOST_URL + "/service.php"

// Global tint Color

let FD_COLOR_GLOBAL = UIColor(hue:0.53, saturation:0.51, brightness:0.71, alpha:1)

// MENU Cache key

let FD_CATEGORIES_CACHE_KEY = "FD_CATEGORIES_CACHE_KEY"

// Error domain

let FD_ERROR_DOMAIN = "FD_ERROR_DOMAIN"

var FD_DEFAULT_CATEGORY: Int = 0
