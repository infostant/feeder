//
//  NSCache-Extension.swift
//  billboard
//
//  Created by Nattawut Singhchai on 10/29/15.
//  Copyright © 2015 2bsimple. All rights reserved.
//

import UIKit

extension NSCache {
    subscript(key: AnyObject) -> AnyObject? {
        get {
            return objectForKey(key)
        }
        set {
            if let value: AnyObject = newValue {
                setObject(value, forKey: key)
            } else {
                removeObjectForKey(key)
            }
        }
    }
}
