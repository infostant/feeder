//
//  DetailViewController.swift
//  Feeder
//
//  Created by Nattawut Singhchai on 12/1/15.
//  Copyright © 2015 2bsimple. All rights reserved.
//

import UIKit
import GoogleMobileAds
import Social
import SafariServices
import JSQWebViewController
import CoreLocation
import MapKit

class DetailViewController: UIViewController , UIWebViewDelegate,GADInterstitialDelegate {

    @IBOutlet weak var detailDescriptionLabel: UILabel!

    @IBOutlet weak var webView: UIWebView?
    
    @IBOutlet weak var tmpViewTop: UIView!
    
    @IBOutlet weak var tmpViewBottom: UIView!
    
    @IBOutlet weak var activity: UIActivityIndicatorView!
    
    @IBOutlet weak var layeredView: UIView!
    
    @IBOutlet weak var adsView: GADBannerView!
    
    var obj:HomePost? {
        didSet {
            if let obj = obj {
                title = obj.titleString
                if let webView = webView {
                    if let rightBar = navigationItem.rightBarButtonItem {
                        rightBar.enabled = true
                    }
                    webView.loadRequest(NSURLRequest(URL: obj.linkURL!))
                    UIApplication.sharedApplication().networkActivityIndicatorVisible = true
                    layeredView.hidden = false
                    activity.startAnimating()
                    tryShowInterstitilAds()
                }
            }else{
                if let rightBar = navigationItem.rightBarButtonItem {
                    rightBar.enabled = false
                }
            }
        }
    }

    var detailItem: AnyObject? {
        didSet {
            // Update the view.
            self.configureView()
        }
    }

    func configureView() {
        // Update the user interface for the detail item.
        if let detail = self.detailItem {
            if let label = self.detailDescriptionLabel {
                label.text = detail.description
            }
        }
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.rightBarButtonItem?.enabled = false
        // Do any additional setup after loading the view, typically from a nib.
        self.configureView()
        
        tmpViewTop.backgroundColor = UIColor.mainColor().colorWithAlphaComponent(0.3)
        let img = UIImage.tmpLineWithColor(UIColor.mainColor().colorWithAlphaComponent(0.05),lineSpace: 12)
        tmpViewBottom.backgroundColor = UIColor(patternImage:img)
        
        if let url = obj?.linkURL {
            webView?.loadRequest(NSURLRequest(URL: url))
            UIApplication.sharedApplication().networkActivityIndicatorVisible = true
            activity.startAnimating()
            navigationItem.rightBarButtonItem?.enabled = true
            tryShowInterstitilAds()
        }
        adsView.startRequest(self)
    }
    
    var interstitial:GADInterstitial?
    
    func tryShowInterstitilAds() {
         let count = NSUserDefaults.standardUserDefaults().integerForKey("adsCount")
        if count % 5 == 0 {
            interstitial = GADInterstitial(adUnitID: "ca-app-pub-1427553436012356/2273516626")
            interstitial?.delegate = self
            interstitial?.loadRequest(GADRequest())
        }
        NSUserDefaults.standardUserDefaults().setInteger(1+count, forKey: "adsCount")
    }
    
    func interstitialDidReceiveAd(ad: GADInterstitial!) {
        if ad.isReady {
            ad.presentFromRootViewController(navigationController!)
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        navigationItem.leftBarButtonItem?.tintColor = UIColor.whiteColor()
        navigationItem.rightBarButtonItem?.tintColor = UIColor.whiteColor()
        
        if let inset = webView?.scrollView.contentInset {
            let rst = UIEdgeInsets(top: inset.top, left: inset.left, bottom: bottomLayoutGuide.length + 50, right: inset.right)
            webView?.scrollView.contentInset = rst
            webView?.scrollView.scrollIndicatorInsets = rst
        }
        
    }

    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func webViewDidStartLoad(webView: UIWebView) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
        activity.stopAnimating()
        layeredView.hidden = true
    }

    @IBAction func share(sender: UIBarButtonItem) {

        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        alert.addAction("Share", style: .Default) { _ -> Void in
            
            let textToShare = self.obj?.titleString
            
            if let myWebsite = self.obj?.linkURL
            {
                let activityVC = UIActivityViewController(activityItems: [textToShare!, myWebsite], applicationActivities: nil)
                activityVC.navigationController?.navigationBar.tintColor = UIColor.mainColor()
                if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Phone {
                    self.presentViewController(activityVC, animated: true, completion: nil)
                }
                else {
                    if #available(iOS 9.0, *) {
                        activityVC.popoverPresentationController?.sourceView = self.view
                        activityVC.popoverPresentationController?.barButtonItem = sender
                        self.presentViewController(activityVC, animated: true, completion: nil)
                    } else {
                        let popup: UIPopoverController = UIPopoverController(contentViewController: activityVC)
                        popup.presentPopoverFromBarButtonItem(sender, permittedArrowDirections: .Up, animated: true)
                    }
                }
            }
        }
        
        print(obj?.location)
        
        if let coordinate = obj?.location {
            alert.addAction("Open in Maps", style: .Default, handler: { _ -> Void in
                let placeMark = MKPlacemark(coordinate: coordinate, addressDictionary: nil)
                let mapItem = MKMapItem(placemark: placeMark)
                mapItem.name = self.obj?.titleString
                mapItem.openInMapsWithLaunchOptions([MKLaunchOptionsDirectionsModeKey:MKLaunchOptionsDirectionsModeDriving])
            })
            
            if (UIApplication.sharedApplication().canOpenURL(NSURL(string:"comgooglemaps://")!)) {
                alert.addAction("Open in Google Maps", style: .Default, handler: {_ -> Void in
                    UIApplication.sharedApplication().openURL(NSURL(string:
                        "comgooglemaps://?saddr=&daddr=\(coordinate.latitude),\(coordinate.longitude)&directionsmode=driving")!)
                })
            }
        
        }
        
        alert.addAction("Cancel", style: .Cancel, handler: nil)
        if let presenter = alert.popoverPresentationController {
            presenter.barButtonItem = sender
        }
        presentViewController(alert, animated: true, completion: nil)
    }
    
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        print(request.URL)
        
        
        if let host = request.URL?.host {
            //            print(request.URL?.lastPathComponent)
            //            print(host)
            if (host == "m.facebook.com" || host == "facebook.com" || host == "www.facebook.com") && request.URL?.lastPathComponent == "sharer.php" {
                return true
//                if let shareURL = request.URL?.queryDictionary["u"] {
//                    let content = FBSDKShareLinkContent()
//                    content.contentURL = NSURL(string: shareURL)
//                    let alert = UIAlertController(title: shareURL, message: nil, preferredStyle: .ActionSheet)
//                    alert.addAction(UIAlertAction(title: "Write Post", style: .Default, handler: { (action) -> Void in
//                        FBSDKShareDialog.showFromViewController(self, withContent: content, delegate: nil)
//                    }))
//                    
//                    alert.addAction(UIAlertAction(title: "Send in Messenger", style: .Default, handler: { (action) -> Void in
//                        FBSDKMessageDialog.showWithContent(content, delegate: nil)
//                    }))
//                    
//                    alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil))
//                    
//                    presentViewController(alert, animated: true, completion: nil)
//                    
//                    return false
//                }
                
            }else if (host == "twitter.com") && request.URL?.lastPathComponent == "tweet" {
                guard SLComposeViewController.isAvailableForServiceType(SLServiceTypeTwitter) else {
                    return true
                }
                guard let component = request.URL?.queryDictionary else {
                    return true
                }
                
                let share = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
                share.setInitialText(component["text"])
                if let linkString = component["url"] {
                    share.addURL(NSURL(string: linkString))
                }
                
                
                presentViewController(share, animated: true, completion: nil)
                return false
                
            }else if (host == "plus.google.com") && request.URL?.lastPathComponent == "share" {
                if let url = request.URL {
                    openWebViewForURL(url)
                }
                return false
            }
            else if (host == FD_HOST_URL) {
                return true
            }else{
                print("except:\(host)")
            }
            guard host != "www.youtube.com" && host != "youtube.com" && host != "youtu.be" && host != "platform.twitter.com" else {
                return true
            }
            if navigationType == .Other {
                return true
            }
            
            
            if let url = request.URL {
                openWebViewForURL(url)
                return false
            }
            
        }
        return true
    }
    
    func openWebViewForURL(URL:NSURL) {
        if #available(iOS 9.0, *) {
            let vc = SFSafariViewController(URL: URL)
            presentViewController(vc, animated: true, completion: nil)
        } else {
            let controller = WebViewController(url: URL)
            let nav = UINavigationController(rootViewController: controller)
            presentViewController(nav, animated: true, completion: nil)
        }
    }
}

extension NSURL {
    
    var queryDictionary: [String:String] {
        let components = self.query?.componentsSeparatedByString("&")
        var dictionary = [String:String]()
        
        for pairs in components ?? [] {
            let pair = pairs.componentsSeparatedByString("=")
            if pair.count == 2 {
                dictionary[pair[0].stringByRemovingPercentEncoding!] = pair[1].stringByRemovingPercentEncoding!
            }
        }
        return dictionary
    }
    
}


