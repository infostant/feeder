//
//  String-Extension.swift
//  Feeder
//
//  Created by Nattawut Singhchai on 12/7/15.
//  Copyright © 2015 2bsimple. All rights reserved.
//

import UIKit

extension String {
    func escapedHTMLString() -> String {
        let encodedData = dataUsingEncoding(NSUTF8StringEncoding)!
        let attributedOptions : [String: AnyObject] = [
            NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
            NSCharacterEncodingDocumentAttribute: NSUTF8StringEncoding
        ]
        do {
            let attributedString = try NSAttributedString(data: encodedData, options: attributedOptions, documentAttributes: nil)
            return attributedString.string
        } catch {
            return self
        }
    }

}
