//
//  UIFont+CSPraJad.swift
//  Feeder
//
//  Created by Nattawut Singhchai on 12/2/15.
//  Copyright © 2015 2bsimple. All rights reserved.
//

import UIKit

extension UIFont {
    
    class func CSPraJadFamily() -> [String] {
        return UIFont.fontNamesForFamilyName("CS PraJad")
    }
    
    class func CSPraJadWithSize(size: CGFloat) -> UIFont {
        return UIFont.CSPraJadRegularWithSize(size)
    }
    
    class func CSPraJadRegularWithSize(size: CGFloat) -> UIFont {
        return UIFont(name: "CSPraJad", size: size)!
    }
    
    class func CSPraJadItalicWithSize(size: CGFloat) -> UIFont {
        return UIFont(name: "CSPraJad-Italic", size: size)!
    }
    
    class func CSPraJadBoldWithSize(size: CGFloat) -> UIFont {
        return UIFont(name: "CSPraJad-bold", size: size)!
    }
    
    class func CSPraJadBoldItalicWithSize(size: CGFloat) -> UIFont {
        return UIFont(name: "CSPraJad-boldItalic", size: size)!
    }
}
