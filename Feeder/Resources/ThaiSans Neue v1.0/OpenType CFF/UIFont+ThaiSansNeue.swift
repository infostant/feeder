//
//  UIFont+ThaiSansNeue.swift
//  Feeder
//
//  Created by Nattawut Singhchai on 12/2/15.
//  Copyright © 2015 2bsimple. All rights reserved.
//

import UIKit

extension UIFont {
    
    class func ThaiSansNeueBlackWithSize(size: CGFloat) -> UIFont {
        return UIFont(name: "ThaiSansNeue-Black", size: size)!
    }
}
