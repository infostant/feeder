//
//  SelectableTitleView.swift
//  Feeder
//
//  Created by Nattawut Singhchai on 12/21/15.
//  Copyright © 2015 2bsimple. All rights reserved.
//

import UIKit

class SelectableTitleView: UIButton {
    
    @IBOutlet internal weak var titleLbl: UILabel!
    @IBOutlet internal weak var subLbl: UILabel!
    
    var onTouched: (AnyObject->Void)?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        addTarget(self, action: "touched:", forControlEvents: .TouchUpInside)
    }
    
    func touched(sender:AnyObject) {
        self.onTouched?(sender)
    }
    
    var title:String? {
        didSet {
            titleLbl.text = title
        }
    }
    
    var subtitle:String? {
        didSet {
            subLbl.text = (subtitle ?? "Categories") + " ▾"
        }
    }
    
    class func Nib() -> UINib? {
        return UINib(nibName: "SelectableTitleView", bundle: NSBundle.mainBundle())
    }

    class func instanceFromNib() -> SelectableTitleView {
        let s = UINib(nibName: "SelectableTitleView", bundle: nil).instantiateWithOwner(nil, options: nil)[0] as! SelectableTitleView
        s.showsTouchWhenHighlighted = true
        return s
    }

}
