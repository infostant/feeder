//
//  FeedViewController.swift
//  Feeder
//
//  Created by Nattawut Singhchai on 12/1/15.
//  Copyright © 2015 2bsimple. All rights reserved.
//

import UIKit
import CollectionViewWaterfallLayout
import LoremIpsum
import AlamofireImage
import Foundation
import GoogleMobileAds
import CoreLocation

let HEADING_NOTIFIATION = "HEADING_NOTIFIATION"

extension HomePost {
    
    func attributedDetailString() -> NSAttributedString {
        let mt = NSMutableAttributedString(string: titleString!.uppercaseString, attributes: [NSForegroundColorAttributeName:UIColor.mainColor().colorWithAlphaComponent(isTmp ? 0 : 1),NSFontAttributeName:UIFont.ThaiSansNeueBlackWithSize(16)])
        if let descriptionString = descriptionString {
            mt.appendAttributedString(NSAttributedString(string: "\n" + descriptionString, attributes: [NSForegroundColorAttributeName:UIColor(hue: 237.0/360.0, saturation: 0.05, brightness: 0.47, alpha: isTmp ? 0 : 1),NSFontAttributeName:UIFont.CSPraJadWithSize(12)]))
        }
        return mt
    }
    
    class func tmpWithNumber(count: Int) -> [HomePost] {
        var a = [HomePost]()
        let heightList = [600,800,1024,400]
        for _ in 1...count {
            let h = HomePost()
            h.titleString = LoremIpsum.word()
            h.descriptionString = LoremIpsum.sentence()
            h.imageSize = CGSize(width: 800, height: heightList[Int(arc4random_uniform(UInt32(heightList.count)))])
            h.imageURL = NSURL(string: "http://localhost")
            h.isTmp = true
            a.append(h)
        }

        return a
    }
    
    func randInRange(range: Range<Int>) -> Int {
        return  Int(arc4random_uniform(UInt32(range.endIndex - range.startIndex))) + range.startIndex
    }
}

class FeedCell: UICollectionViewCell {
    
    @IBOutlet weak var locationContainer: UIVisualEffectView!
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var detailLabel: UILabel!
    
    @IBOutlet weak var detailHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var imageRatio: NSLayoutConstraint!
    
    @IBOutlet weak var distanceLabel: UILabel!
    
    @IBOutlet weak var directionImageView: UIImageView!
}

class FeedViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var adsView: GADBannerView!


    var _currentLocation: CLLocationCoordinate2D?
    
    var currentLocation: CLLocationCoordinate2D? {
        set {
            if _currentLocation == nil && newValue != nil {
                reloadData(nil)
            }
            _currentLocation = newValue
        }get{
            return _currentLocation
        }
    }
    
    var refreshControl = UIRefreshControl()
    
    var detailViewController: DetailViewController? = nil
    
    var objects = [HomePost]()
    var canLoadMore = true
    var page: Int = 1
    var isReset = true
    var isLoading:Bool = false {
        didSet {
            UIApplication.sharedApplication().networkActivityIndicatorVisible = isLoading
        }
    }
    
    var titleView: SelectableTitleView?
    
    var cacheHeight = [Int: CGFloat]()
    
    private let insets = UIEdgeInsetsMake(6, 6, 6, 6)
    
    private let column = UIScreen.mainScreen().traitCollection.userInterfaceIdiom == .Pad ? 1 : 2
    
    var locationManager:CLLocationManager?
    
    let useLocationBase = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if useLocationBase {
            locationManager = CLLocationManager()
            locationManager?.delegate = self
            locationManager?.desiredAccuracy = kCLLocationAccuracyBest
            locationManager?.requestWhenInUseAuthorization()
            locationManager?.startUpdatingLocation()
            locationManager?.headingFilter = 0.5
            locationManager?.startUpdatingHeading()
        }
        
        category = Category(name: "", id: FD_DEFAULT_CATEGORY)
        
        if 0 == FD_DEFAULT_CATEGORY {
            titleView = SelectableTitleView.instanceFromNib()
            titleView?.onTouched = {  self.changeCategory($0) }
            navigationItem.titleView = titleView
        }
        
        objects += HomePost.tmpWithNumber(5)
        // Do any additional setup after loading the view, typically from a nib.
        refreshControl.tintColor = UIColor.mainColor()
        collectionView.addSubview(refreshControl)
        refreshControl.addTarget(self, action: "reloadData:", forControlEvents: UIControlEvents.ValueChanged)
        
        if let split = self.splitViewController {
            let controllers = split.viewControllers
            self.detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }
        
        layout.columnCount = column
        layout.minimumColumnSpacing = 6
        layout.minimumInteritemSpacing = 6
        layout.sectionInset = insets
        
        adsView.startRequest(self)
        
        if !useLocationBase {
            reloadData(nil)
        }
    }

    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        
        let inset = collectionView.contentInset
        collectionView.contentInset = UIEdgeInsets(top: inset.top, left: inset.left, bottom: bottomLayoutGuide.length + 50, right: inset.right)
        collectionView.scrollIndicatorInsets = collectionView.contentInset
    }
    
    func loadMore() {
        print("loadMore")
        isLoading = true
        Feed.loadPage(page++, category: category,coordinate: locationManager?.location?.coordinate, callback: {
            self.isLoading = false
            switch $0 {
            case .Success(let value):
                if self.isReset || self.page <= 2 {
                    self.objects.removeAll()
                }
                self.objects += value.data
                if  self.useLocationBase {
                    self.objects = self.sortByNearby(self.objects)
                }
                self.canLoadMore = value.canLoadMore
                self.collectionView.reloadData()
                if self.isReset && self.objects.count > 0  && UIScreen.mainScreen().traitCollection.userInterfaceIdiom == .Pad {
                    if self.splitViewController?.viewControllers.count > 1 {
                        let nvc = self.splitViewController?.viewControllers.last as! UINavigationController
                        if let controller = nvc.viewControllers.first as? DetailViewController {
                            controller.obj = self.objects.first
                            controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
                            controller.navigationItem.leftItemsSupplementBackButton = true
                        }
                    }
                }
                
            case .Failure(let error):
                print(error)
                self.canLoadMore = false
                
            }
            self.isReset = false
            if self.refreshControl.refreshing {
                self.refreshControl.endRefreshing()
            }
        })
    }
    
    var category: Category? {
        didSet {
            if let category = category {
                titleView?.subtitle = category.id == nil ? nil : category.name
                reloadData(nil)
            }
        }
    }
    
    @IBAction func changeCategory(sender: AnyObject) {
        print(sender)
        let nav = storyboard!.instantiateViewControllerWithIdentifier("categories") as! UINavigationController
        if let catVC = nav.viewControllers.first as? CategoryViewController {
            catVC.didSelectedCategory = { category in
                self.category = category
            }
            presentViewController(nav, animated: true, completion: nil)
        }
    }
    
    func reloadData(sender: UIRefreshControl?) {
        print("**RELOAD DATA**")
        isReset = true
        page = 1
        loadMore()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    var layout: CollectionViewWaterfallLayout {
        return collectionView?.collectionViewLayout as! CollectionViewWaterfallLayout
    }
    
    // MARK: - Segues
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        if let cell = sender as? UICollectionViewCell {
            guard let indexPath = collectionView.indexPathForCell(cell) else {
                return false
            }
            let object = objects[indexPath.row]
        
            guard let _ = object.linkURL else {
                return false
            }
            
            if UIScreen.mainScreen().traitCollection.userInterfaceIdiom == .Pad {
                if splitViewController?.viewControllers.count > 1 {
                    let nvc = splitViewController?.viewControllers.last as! UINavigationController
                    if let controller = nvc.viewControllers.first as? DetailViewController {
                        controller.obj = object
                        controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
                        controller.navigationItem.leftItemsSupplementBackButton = true
                        return false
                    }
                }
            }
            return !object.isTmp
        }
        return false
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let cell = sender as? UICollectionViewCell {
            guard let indexPath = collectionView.indexPathForCell(cell) else {
                return
            }
            let object = objects[indexPath.row]
            
            guard let _ = object.linkURL else {
                return
            }
        
            if segue.identifier == "showDetail" {
                if let _ = self.collectionView.indexPathsForSelectedItems()?.first {
                    let controller = (segue.destinationViewController as! UINavigationController).topViewController as! DetailViewController
                    controller.obj = object
                    controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
                    controller.navigationItem.leftItemsSupplementBackButton = true
                }
            }
        }
    }
    
    override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
        collectionView.reloadData()
    }
    
    
    func sortByNearby(var array:[HomePost]) -> [HomePost]{
        if let currentLocation = currentLocation {
            array.sortInPlace {
                let currentLocation = CLLocation(latitude: currentLocation.latitude, longitude: currentLocation.longitude)
                if let aLoc = $0.location,bLoc = $1.location {
                    let distanceA = currentLocation.distanceFromLocation(CLLocation(latitude: aLoc.latitude, longitude: aLoc.longitude))
                    let distanceB = currentLocation.distanceFromLocation(CLLocation(latitude: bLoc.latitude, longitude: bLoc.longitude))
                    return distanceA < distanceB
                }else{
                    return false
                }
                
            }
        }
        return array
    }
}

extension FeedViewController: UICollectionViewDelegate, UICollectionViewDataSource  {
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return objects.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! FeedCell
        let obj = objects[indexPath.row]
        
        cell.imageView.af_setImageWithURL(obj.imageURL!, placeholderImage: UIImage(color:UIColor.mainColor().colorWithAlphaComponent(0.1)))
        cell.detailLabel.attributedText = obj.attributedDetailString()

        cell.detailLabel.backgroundColor = obj.isTmp ? UIColor(patternImage: UIImage.tmpLineWithColor(UIColor.mainColor().colorWithAlphaComponent(0.05))) : UIColor.clearColor()
        
        let height = detailHeightForAttributedString(obj.attributedDetailString())
        cell.detailHeightConstraint.constant = ceil( height )
        cell.contentView.backgroundColor = UIColor.whiteColor()
        
        cell.layer.cornerRadius = 2.0
        cell.clipsToBounds = true
        
        if canLoadMore && (indexPath.row > (objects.count - 2)) && !isLoading && !obj.isTmp {
            loadMore()
        }
        if let poiLocation = obj.location, currentLocation = currentLocation {
            cell.locationContainer.hidden = false
            let objLocation = CLLocation(latitude: poiLocation.latitude, longitude: poiLocation.longitude)
            let distance = objLocation.distanceFromLocation(CLLocation(latitude: currentLocation.latitude, longitude: currentLocation.longitude))
            if distance < 1000 {
                cell.distanceLabel.text = String.localizedStringWithFormat("%.0f m", distance)
            }else{
                cell.distanceLabel.text = String.localizedStringWithFormat("%.1f km", distance / 1000)
            }
            
            if let heading = locationManager?.heading {
                let angle = currentLocation.angleToCoordinate(poiLocation)
                var additionalAngle:CGFloat = 0
                switch UIApplication.sharedApplication().statusBarOrientation {
                case .LandscapeRight: additionalAngle = 90
                case .PortraitUpsideDown: additionalAngle = 180
                case .LandscapeLeft: additionalAngle = 270
                default: break
                }
                
                var finalDegs = CGFloat(heading.trueHeading) + additionalAngle - angle + 45
                if finalDegs < 0 {
                    finalDegs += 360.0
                }else if finalDegs > 360 {
                    finalDegs -= 360.0
                }
                cell.directionImageView.transform = CGAffineTransformMakeRotation(CLLocationDegrees(finalDegs).radians * -1)
                
            }else{
                cell.directionImageView.transform = CGAffineTransformMakeRotation(0)
            }
            
        }else{
            cell.locationContainer.hidden = true
        }
        
        return cell
    }
    
    func detailHeightForAttributedString(str:NSAttributedString) -> CGFloat {
        return  str.boundingRectWithSize(CGSize(width: cellWidth() - 12, height: CGFloat.max), options: [.UsesLineFragmentOrigin,.UsesFontLeading], context: nil).height
    }
    
}

extension FeedViewController: CollectionViewWaterfallLayoutDelegate {
    
    func cellWidth() -> CGFloat {
        let widthWithoutSideGap:CGFloat = (collectionView.frame.width - (self.layout.sectionInset.left + self.layout.sectionInset.right))
        let innerGap:CGFloat = CGFloat(self.layout.minimumInteritemSpacing * Float(self.layout.columnCount - 1))
        return floor(( widthWithoutSideGap - innerGap ) / CGFloat(self.layout.columnCount))
    }
    
    func collectionView(collectionView: UICollectionView, layout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let obj = objects[indexPath.row]
        let size = obj.imageSize!.scaleToWidth(cellWidth() - 12)
        var textHeight = detailHeightForAttributedString(obj.attributedDetailString())
        textHeight = textHeight > 200 ? 200 : textHeight
        let  height = 18 + ceil(size.height + textHeight)

        return CGSize(width: size.width + 12, height: height)
    }
}

extension FeedViewController: CLLocationManagerDelegate {
    

    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        currentLocation = manager.location?.coordinate
    }

    func locationManager(manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        if let cells = collectionView.visibleCells() as? [FeedCell] {
            for cell in cells {
                if let indexPath = collectionView.indexPathForCell(cell) {
                    let obj = objects[indexPath.row]
                    if let poiLocation = obj.location, currentLocation = manager.location {
                        
                        let objLocation = CLLocation(latitude: poiLocation.latitude, longitude: poiLocation.longitude)
                        let distance = objLocation.distanceFromLocation(currentLocation)
                        if distance < 1000 {
                            cell.distanceLabel.text = String.localizedStringWithFormat("%.0f m", distance)
                        }else{
                            cell.distanceLabel.text = String.localizedStringWithFormat("%.2f km", distance / 1000)
                        }
                        
                        let angle = currentLocation.coordinate.angleToCoordinate(poiLocation)
                        var additionalAngle:CGFloat = 0
                        switch UIApplication.sharedApplication().statusBarOrientation {
                        case .LandscapeRight: additionalAngle = 90
                        case .PortraitUpsideDown: additionalAngle = 180
                        case .LandscapeLeft: additionalAngle = 270
                        default: break
                        }
                        
                        var finalDegs = CGFloat(newHeading.trueHeading) + additionalAngle - angle + 45
                        if finalDegs < 0 {
                            finalDegs += 360.0
                        }else if finalDegs > 360 {
                            finalDegs -= 360.0
                        }
                        cell.directionImageView.transform = CGAffineTransformMakeRotation(CLLocationDegrees(finalDegs).radians * -1)
                    }
                }
            }
        }
    }
}

extension CLLocationDegrees {
    var radians : CGFloat {
        return CGFloat(self) * CGFloat(M_PI) / 180.0
    }
}

extension CGFloat {
    var degrees: CGFloat {
        return CGFloat(self) * 180.0 / CGFloat(M_PI)
    }
}

extension CLLocationCoordinate2D {
    func angleToCoordinate(coordinate: CLLocationCoordinate2D) -> CGFloat {
        let fLat = latitude.radians
        let fLng = longitude.radians
        let tLat = coordinate.latitude.radians
        let tLng = coordinate.longitude.radians
        
        let degree = (atan2(sin(tLng-fLng)*cos(tLat), cos(fLat)*sin(tLat)-sin(fLat)*cos(tLat)*cos(tLng-fLng))).degrees
        
        if (degree >= 0) {
            return degree;
        } else {
            return 360+degree;
        }
    }
}
