//
//  SlideNavigationController.swift
//  Feeder
//
//  Created by Nattawut Singhchai on 12/9/15.
//  Copyright © 2015 2bsimple. All rights reserved.
//

import UIKit

class SlideNavigationController: UINavigationController,UIViewControllerTransitioningDelegate {
    
    var maxWidth:CGFloat = 280
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        modalPresentationStyle = .Custom
        transitioningDelegate = self
    }
    
    func presentationControllerForPresentedViewController(presented: UIViewController, presentingViewController presenting: UIViewController, sourceViewController source: UIViewController) -> UIPresentationController? {
        if self == presented {
            return SlidePresentationController(presentedViewController: presented, presentingViewController: presenting, backgroundColor: UIColor.mainColor())
        }
        return nil
    }
    
    func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        if self == presented {
            let animation = SlideAnimationController(presenting: true)
//            animation.maxWidth = maxWidth
            animation.direction = .Top
            animation.duration = 0.5
            animation.topInset = 44
            return animation
        }
        return nil
    }
    
    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        if self == dismissed {
            let animation = SlideAnimationController(presenting: false)
//            animation.maxWidth = maxWidth
            animation.direction = .Top
            animation.duration = 0.5
            animation.topInset = 44
            return animation
        }
        return nil
    }
    
}
