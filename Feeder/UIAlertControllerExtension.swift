//
//  UIAlertControllerExtension.swift
//  Feeder
//
//  Created by Nattawut Singhchai on 1/6/16.
//  Copyright © 2016 2bsimple. All rights reserved.
//

import UIKit

extension UIAlertController {
    func addAction(title: String?,style: UIAlertActionStyle,handler: (UIAlertAction -> Void)?) {
        self.addAction(UIAlertAction(title: title, style: style, handler: handler))
    }
}
