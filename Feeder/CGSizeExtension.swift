//
//  CGSizeExtension.swift
//  Feeder
//
//  Created by Nattawut Singhchai on 12/1/15.
//  Copyright © 2015 2bsimple. All rights reserved.
//

import UIKit

extension CGSize {
    
    var ratio:CGFloat {
        return width/height
    }
    
    func scaleToWidth(w:CGFloat) -> CGSize {
        return CGSizeMake(w, w / ratio)
    }
    
}
