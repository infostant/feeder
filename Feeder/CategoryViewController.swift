//
//  CategoryViewController.swift
//  Feeder
//
//  Created by Nattawut Singhchai on 12/9/15.
//  Copyright © 2015 2bsimple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class Category {
    
    var name:String
    var id:Int?
    
    init(name: String,id: Int?){
        self.name = name
        self.id = id
    }
    
    class func allCategory() -> Category {
        return  Category(name: "All", id: nil)
    }
    
    class func loadPage(callback: (Result<[Category],NSError>)->Void) {
        
        let param:[String:AnyObject] = ["method":"getcategory"]
        
        if let cache = NSCache()[FD_CATEGORIES_CACHE_KEY] {
            callback(.Success(categoriesFromJSON(JSON(cache)["data"].array!)))
        }
        
        Alamofire.request(.GET, FD_SERVICE_URL , parameters: param)
            .responseJSON { r in
                
                print("req: \(r.request?.URLString)")
                switch r.result {
                case .Success(let value):
                    
                    let json = JSON(value)
                    guard let data = json["data"].array else {
                        callback(.Failure(NSError(domain: FD_ERROR_DOMAIN, code: -1, userInfo: [NSLocalizedFailureReasonErrorKey:"Server don't return data",
                            NSLocalizedDescriptionKey:"Cannot connect to server please try again."])))
                        break
                    }
                    
                    NSCache()[FD_CATEGORIES_CACHE_KEY] = value
                    
                    callback(.Success(categoriesFromJSON(data)))

                case .Failure(let error):
                    callback(.Failure(error))
                }
        }
    }
    
    class func categoriesFromJSON(json: [JSON]) -> [Category] {
        var a = [Category]()
        
        for d in json {
            if d["name"].string?.uppercaseString != "ALL" {
                a.append(Category(name: d["name"].string!, id: d["catid"].int!))
            }
        }
        return a
    }
}

class CategoryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!

    @IBOutlet weak var tableHeightConstraint: NSLayoutConstraint!
    
    var categories = [Category]()
    
    var didSelectedCategory:(Category->Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        updateHeight()
        
        Category.loadPage {
            switch $0 {
            case .Success(let _cats):
                self.categories.removeAll()
                self.categories.append(Category.allCategory())
                self.categories += _cats
                self.tableView.reloadData()
                self.updateHeight()
            case .Failure(let error):
                print(error)
            }
        }
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "handleTap:"))
    }
    func updateHeight() {
        NSOperationQueue.mainQueue().addOperationWithBlock {
            let shouldBeHeight =   self.topLayoutGuide.length + self.tableView.contentSize.height
            self.tableHeightConstraint.constant = shouldBeHeight > self.view.frame.height ? self.view.frame.height : shouldBeHeight
        }
    }
    
    func handleTap(sender: UITapGestureRecognizer) {
        if sender.state == .Ended {
            print(sender.view)
            dismissViewControllerAnimated(true, completion: nil)
        }
        sender.cancelsTouchesInView = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

     func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return categories.count == 0 ? 1 : categories.count
    }

    
     func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if categories.count == 0 {
            return tableView.dequeueReusableCellWithIdentifier("loading", forIndexPath: indexPath)
        }
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)

        // Configure the cell...
        cell.textLabel?.text = categories[indexPath.row].name

        return cell
    }
    
     func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        guard categories.count > 0 else {
            return
        }
        if let cb = didSelectedCategory {
            cb(categories[indexPath.row])
        }
        dismissViewControllerAnimated(true, completion: nil)
    }
    
     func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func scrollViewDidScroll(scrollView :UIScrollView)
    {
        if (scrollView.contentOffset.y<=0) {
        scrollView.contentOffset = CGPointZero;
        }
    }
}


