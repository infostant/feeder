//
//  GoogleAdsExtension.swift
//  Feeder
//
//  Created by Nattawut Singhchai on 12/8/15.
//  Copyright © 2015 2bsimple. All rights reserved.
//

import GoogleMobileAds

extension GADBannerView {
    func startRequest(rootViewController: UIViewController){
        let request = GADRequest()
        request.keywords = ["medical","medicine","bangkok","Thailand"]
        layoutIfNeeded()
//        adSize = kGADAdSizeSmartBannerPortrait
        self.rootViewController = rootViewController

        request.testDevices = [ kGADSimulatorID ]
        loadRequest(request)
    }
}
